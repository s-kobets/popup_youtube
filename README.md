## Usage

### Start development
```
npm start
```

### Build development
```
npm build:dev
```

### Build production
```
npm build
```
