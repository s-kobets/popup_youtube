/* global */
/* eslint camelcase: ["error", {properties: "never"}] */
import Vue from 'vue';

import vDialog from './component/v-dialog';
import vIframe from './component/v-iframe';

require('../css/main.css');

Vue.component('v-dialog', vDialog);
Vue.component('v-iframe', vIframe);

new Vue({ // eslint-disable-line no-new
	el: '#app'
});
