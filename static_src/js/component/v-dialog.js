/* global document */
const body = document.querySelectorAll('body')[0];

export default {
	template: '#dialog-template',
	data() {
		return {
			active: false
		}
	},
	methods: {
		open() {
			this.active = !this.active;
			body.classList.add('hidden');
			return this.active;
		},
		close() {
			this.active = false;
			body.classList.remove('hidden');
			return false;
		}
	}
}
