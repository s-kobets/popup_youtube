// test/component-a.spec.js
/* eslint-disable */
import Vue from 'vue';
import vDialog from '../../static_src/js/component/v-dialog.js';

describe('v-dialog', () => {

	// проверяем свойства JavaScript-объекта
	it('should have correct active', () => {
		expect(vDialog.data()).toEqual({active: false});
	});

	// проверяем methods open
	it('should have correct open', () => {
		expect(vDialog.methods.open()).toEqual(true);
	});

	// проверяем methods close
	it('should have correct close', () => {
		expect(vDialog.methods.close()).toEqual(false);
	});
});
/* eslint-enable */
